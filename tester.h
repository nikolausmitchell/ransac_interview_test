#ifndef TESTER_H
#define TESTER_H

#include <vector>

class DataGenerator
{
    struct point{
        float x;
        float y;
    };
public:
    std::vector<point> data;
    std::ofstream file;
    std::vector<point> getData();

    void generateData(float a, float c, float noise);

    void writeData();
    void writeAnswer(float a, float c);

    std::vector<float> getXData();
    std::vector<float> getYData();
};


class Tester{
public:
    DataGenerator d;
    Tester();
    void inputAnswer(float a,float c);
};
#endif // TESTER_H

