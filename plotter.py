import numpy as np
import matplotlib.pyplot as plt
import sys

filename=sys.argv[1]

file_object=open(filename,"r")
x=[]
y=[]
a=0
c=0
for line in file_object:
	token = line.split()
	if token[0]== 'Answer':
		a=float(token[1])
		c=float(token[2])
	else:	
		x.append(token[0])
		y.append(token[1])

t = np.arange(0,len(x),1)

answer_x=[]
answer_y=[]
start=a*0-7
end=a*30-7

plt.scatter(x,y)
plt.plot([0,30],[start,end] , '-r', label='RANSAC regressor')

plt.legend
plt.show()



