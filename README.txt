This is the NodeIn ransac coding test. Your goal for this challenge is to code up the RANSAC algorithm and have it find the best model that fits the data.

This model will be a simple line described by the function y=ax+c

main.cpp will be where you need to hook in your code. You are allowed to write any other classes and files that you want, however, it will be your responsibility
to make sure everything works with the CMake file. 

The code will output the data and your answer to a Datapoints.txt file. This can then be viewed using the plotter.py file.
- Command is python ./plotter.py <./filepath>

Bonus Points if one has extra time.
- Create your own function to write data to a .txt file. In this file you should store which points are outliers and inliers.
  Modify the plotter.py to also mark a datapoint as either an outlier or an inlier.


Install these libraries to run:
sudo apt-get install build-essential python-tk python-matplotlib cmake
