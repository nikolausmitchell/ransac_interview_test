cmake_minimum_required (VERSION 2.6)
add_definitions(-std=c++11)
project (ransac_generator)
add_executable(ransac_test main.cpp)

target_link_libraries (ransac_test ${CMAKE_CURRENT_SOURCE_DIR}/libransac_test.a)
